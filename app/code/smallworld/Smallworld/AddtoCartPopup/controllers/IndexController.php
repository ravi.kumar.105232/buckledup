<?php
class Smallworld_AddtoCartPopup_IndexController extends Mage_Core_Controller_Front_Action{
    public function IndexAction() {
      
	  $this->loadLayout();   
	  $this->getLayout()->getBlock("head")->setTitle($this->__("Add to Cart Popup"));
	        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
      $breadcrumbs->addCrumb("home", array(
                "label" => $this->__("Home Page"),
                "title" => $this->__("Home Page"),
                "link"  => Mage::getBaseUrl()
		   ));

      $breadcrumbs->addCrumb("add to cart popup", array(
                "label" => $this->__("Add to Cart Popup"),
                "title" => $this->__("Add to Cart Popup")
		   ));

      $this->renderLayout(); 
	  
    }
}