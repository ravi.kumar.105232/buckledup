<?php
class Smallworld_Anandhome_IndexController extends Mage_Core_Controller_Front_Action{
   
		public function bestpricesAction() 
		{
		     $getproductno=Mage::getStoreConfig('anand_home/bestprice_configuration/no_product_bestprice');
		     $getwidth=Mage::getStoreConfig('anand_home/bestprice_configuration/width_image_bestprice');
		     $getheight=Mage::getStoreConfig('anand_home/bestprice_configuration/height_image_bestprice');
		     $id = $this->getRequest()->getParam('categoryId');
		     $category = Mage::getModel('catalog/category')->load($id);
		
		     $products = $category->getProductCollection()->addAttributeToSelect('*')->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED));
			
			 Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($products);
			
		     $products->getSelect()->order(new Zend_Db_Expr('RAND()'));
		     $products->getSelect()->limit($getproductno);
		     $res = array();
		       if(!empty($products))
			     {
		          foreach($products as $product)
		           {
		             $_finalPrice = Mage::helper('core')->currency($product->getPrice(),true,false);
		              $specialprice = Mage::helper('core')->currency($product->getSpecialPrice(),true,false);
		              $dt = $product->getSpecialToDate();
		                $t =  date('Y-m-d',strtotime($dt));
		                   $df = $product->getSpecialFromDate();
						         $f =  date('Y-m-d',strtotime($df));
		            
		             $tmp = ["name" => $product->getName(), "image" => $product->getImageUrl(),"imgwidth" =>
		             $getwidth,"imgheight" =>$getheight ,"price" =>$_finalPrice,"producturl" =>$product->getProductUrl(), "spetodate" =>$t,"spefromdate" => $f,"specialprice" => $specialprice];
		             array_push($res,$tmp);
		            }
		        }
		          
		             echo json_encode($res);

		}
		public function bestsellerAction() 
		{
		     $getproductno=Mage::getStoreConfig('anand_home/bestseller_configuration/no_product_bestseller');
		     $getwidth=Mage::getStoreConfig('anand_home/bestseller_configuration/width_image_bestseller');
		     $getheight=Mage::getStoreConfig('anand_home/bestseller_configuration/height_image_bestseller');
		     $id = $this->getRequest()->getParam('categoryId');
		     $category = Mage::getModel('catalog/category')->load($id);
		
		     $products = $category->getProductCollection()->addAttributeToSelect('*')->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED));
			
			 Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($products);
			
		     $products->getSelect()->order(new Zend_Db_Expr('RAND()'));
		     $products->getSelect()->limit($getproductno);
		     $res = array();
		       if(!empty($products))
			     {
			     	
		          foreach($products as $product)
		           {
		             $_finalPrice = Mage::helper('core')->currency($product->getPrice(),true,false);
		              $specialprice = Mage::helper('core')->currency($product->getSpecialPrice(),true,false);
		              $dt = $product->getSpecialToDate();
		                $t =  date('Y-m-d',strtotime($dt));
		                   $df = $product->getSpecialFromDate();
						         $f =  date('Y-m-d',strtotime($df));
		            
		             $tmp = ["name" => $product->getName(), "image" => $product->getImageUrl(),"imgwidth" =>
		             $getwidth,"imgheight" =>$getheight ,"price" =>$_finalPrice,"producturl" =>$product->getProductUrl(), "spetodate" =>$t,"spefromdate" => $f,"specialprice" => $specialprice];
		             array_push($res,$tmp);
		            }
		        }
		          
		             echo json_encode($res);

		}
		public function suggestAction() 
		{
		     $getproductno=Mage::getStoreConfig('anand_home/suggest_configuration/no_product_suggest');
		     $getwidth=Mage::getStoreConfig('anand_home/suggest_configuration/width_image_suggest');
		     $getheight=Mage::getStoreConfig('anand_home/suggest_configuration/height_image_suggest');
		     $id = $this->getRequest()->getParam('categoryId');
		     $category = Mage::getModel('catalog/category')->load($id);
		
		     $products = $category->getProductCollection()->addAttributeToSelect('*')->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED));
			
			 Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($products);
			
		     $products->getSelect()->order(new Zend_Db_Expr('RAND()'));
		     $products->getSelect()->limit($getproductno);
		     $res = array();
		       if(!empty($products))
			     {
		          foreach($products as $product)
		           {
		             $_finalPrice = Mage::helper('core')->currency($product->getPrice(),true,false);
		              $specialprice = Mage::helper('core')->currency($product->getSpecialPrice(),true,false);
		              $dt = $product->getSpecialToDate();
		                $t =  date('Y-m-d',strtotime($dt));
		                   $df = $product->getSpecialFromDate();
						         $f =  date('Y-m-d',strtotime($df));
		            
		             $tmp = ["name" => $product->getName(), "image" => $product->getImageUrl(),"imgwidth" =>
		             $getwidth,"imgheight" =>$getheight ,"price" =>$_finalPrice,"producturl" =>$product->getProductUrl(), "spetodate" =>$t,"spefromdate" => $f,"specialprice" => $specialprice];
		             array_push($res,$tmp);
		            }
		        }
		          
		             echo json_encode($res);

		}
	
  }
