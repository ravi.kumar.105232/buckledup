<?php
if (!class_exists('Mage_Checkout_OnepageController')) {
    include_once(Mage::getModuleDir('controllers', 'Mage_Checkout') . DS . 'OnepageController.php');
}

class IWD_Opc_Checkout_OnepageController extends Mage_Checkout_OnepageController
{

	public function checkRegisteredAction()
	{
		$params=$this->getRequest()->getParams();
		$email=$params["email_address"];
		$customer = Mage::getModel('customer/customer');
		$customer->setWebsiteId(Mage::app()->getWebsite()->getId());
		$customer->loadByEmail($email);

		if($customer->getId())
		{
			echo '1';
			return;
		}
		else
		{
			echo '0';
			return;
		}

	}
	public function iwdregisterAction(){
		$params=$this->getRequest()->getParams();
		$email=$params['email'];
		$fname=$params['firstname'];
		$lname=$params['lastname'];
		$password=$params['password'];
		$websiteId = Mage::app()->getWebsite()->getId();
		$store = Mage::app()->getStore();
		$customer = Mage::getModel("customer/customer");
		$customer->setWebsiteId($websiteId)
			->setStore($store);
		$customer->loadByEmail($email);
		try{
			if($customer->getId())
			{
				$session = Mage::getSingleton('customer/session');
				//$session->setCustomerAsLoggedIn($customer);
				//echo $customer->getId();
				echo '-2';
			}
			else
			{
				$customer = Mage::getModel("customer/customer");
				$customer->setWebsiteId($websiteId)->setStore($store)->setFirstname($fname)->setLastname($lname)->setEmail($email)->setPassword($password);
				$customer->save();
				$session = Mage::getSingleton('customer/session');
				$session->setCustomerAsLoggedIn($customer);
				echo $customer->getId();
			}
		}
		catch (Exception $e) {
			Zend_Debug::dump($e->getMessage());
		}
	}
	public function iwdloginAction(){
		$params=$this->getRequest()->getParams();
		$email=$params["email_address"];
		$password=$params['password'];
		$session= Mage::getSingleton('customer/session');
		try
			{
				$authenticated = $session->login($email, $password);
				echo 1;
			}
		catch(Exception $e)
			{
				echo 0;
			}
	}
    public function indexAction()
    {
        $scheme = Mage::app()->getRequest()->getScheme();
        if ($scheme == 'http') {
            $secure = false;
        } else {
            $secure = true;
        }

        if (Mage::helper('iwd_opc')->isEnable()) {
            $this->_redirect(IWD_Opc_Helper_Data::IWD_OPC_FRONT_NAME, array('_secure' => $secure));
            return;
        } else {
            parent::indexAction();
        }
    }
}
