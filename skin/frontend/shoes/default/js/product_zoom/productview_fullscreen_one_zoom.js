jQuery(document).ready(function(e){

/*Main Thumbnail alignment of click Ends here*/

 var click_triggered = 0;
 var click_close = 1;

var more_view_class; 
var product_shop_class;

var more_additional_class;
var shop_additional_class;

var more_view_style;
var product_shop_style;


jQuery(".full_view_popup .full_view_anchor").click(function(e){

	if(click_close == 1)
	{
		more_view_class = jQuery(".more-views").attr("class");
		product_shop_class = jQuery(".product-shop").attr("class");

		/*
			var more_additional_class = more_view_class.substring(11,more_view_class.length);
			var shop_additional_class = product_shop_class.substring(12,product_shop_class.length);
		*/
		more_view_style = jQuery(".more-views").attr("style");
		product_shop_style = jQuery(".product-shop").attr("style");

		console.log(more_view_style);
		console.log(more_view_class);
		click_close++;

	}

	

	var elem = document.body;
	//var elem = document.getElementById("product-view");
	
	if ((document.fullScreenElement !== undefined && document.fullScreenElement === null) || (document.msFullscreenElement !== undefined && document.msFullscreenElement === null) || (document.mozFullScreen !== undefined && !document.mozFullScreen) || (document.webkitIsFullScreen !== undefined && !document.webkitIsFullScreen)) {
	    if (elem.requestFullScreen) {
	      elem.requestFullScreen();
	    } else if (elem.mozRequestFullScreen) {
	      elem.mozRequestFullScreen();
	    } else if (elem.webkitRequestFullScreen) {
	      elem.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
	    } else if (elem.msRequestFullscreen) {
	      elem.msRequestFullscreen();
	    }
	    var src_value = jQuery(".full_view_anchor .full_view_image").attr("src");
		var replaced_value = src_value.replace(/zoom-in/i,"zoom-out");
		jQuery(".full_view_anchor .full_view_image").attr("src",replaced_value);

		var source_image = jQuery(".product_placeholder_main.active .gallery-image_primary").attr("src");
		jQuery(".product_placeholder_main.active  .gallery-image_primary").show();

	  } else 
	  {
			jQuery(".more-views").attr("class",more_view_class);	
	  	   	jQuery(".more-views").attr("style",more_view_style);	
			jQuery(".product-shop").attr("class",product_shop_class);	
			jQuery(".product-shop").attr("style",product_shop_style);		
	  	   	
			click_close = 0;
			
		    if (document.cancelFullScreen) {

		      document.cancelFullScreen();
		    } else if (document.mozCancelFullScreen) {
	   			
		      document.mozCancelFullScreen();
		    } else if (document.webkitCancelFullScreen) {
	   			
		      document.webkitCancelFullScreen();
		    } else if (document.msExitFullscreen) {
	   				
		      document.msExitFullscreen();
		    }
		    var src_value = jQuery(".full_view_anchor .full_view_image").attr("src");
			var replaced_value = src_value.replace(/zoom-out/i,"zoom-in");
			jQuery(".full_view_anchor .full_view_image").attr("src",replaced_value);
			click_triggered=0;
			jQuery("#inverted-contain .panzoom").panzoom("destroy");
			jQuery("#inverted-contain").hide();
			jQuery("html,body").scrollTop(0);
	  }
		
})

jQuery(".image_thumb").click(function(e){
	var id_get = jQuery(this).attr("data-ref");

	jQuery(".product-image-thumbs li").removeClass("active_thumb");
	jQuery(".product-image-thumbs li a").removeClass("active");

	jQuery(".product_placeholder_main").removeClass("active")
	jQuery("#"+id_get).parents(".product_placeholder_main").addClass("active");

	jQuery(this).addClass("active");
	jQuery(this).parent("li").addClass("active_thumb");
	
});

jQuery(".image_thumb").mouseover(function(e){
	var id_get = jQuery(this).attr("data-ref");

	jQuery(".product-image-thumbs li").removeClass("active_thumb");
	jQuery(".product-image-thumbs li a").removeClass("active");

	jQuery(".product_placeholder_main").removeClass("active")
	jQuery("#"+id_get).parents(".product_placeholder_main").addClass("active");

	jQuery(this).addClass("active");
	jQuery(this).parent("li").addClass("active_thumb");

	var source_image = jQuery(".product_placeholder_main.active #"+id_get).attr("src");
	jQuery(".product_placeholder_main.active #"+id_get).show();
	jQuery(".panzoom").attr("src",source_image);

	jQuery("#inverted-contain .panzoom").panzoom("destroy");
	jQuery("#inverted-contain").hide();
	click_triggered=0;
	
});

var primary_image_height = jQuery(".gallery-image_primary").height();
var primary_image_width = jQuery(".gallery-image_primary").width();

var secondary_image_height = jQuery(".secondary_image_popup_1").height();
var secondary_image_width = jQuery(".secondary_image_popup_1").width();


jQuery(".gallery-image_primary").click(function(event){
		var source_image = jQuery(this).attr("src");

		jQuery(".panzoom").attr("src",source_image);
		jQuery("#inverted-contain").show();
		jQuery(".panzoom").show();

		jQuery(".product-image-gallery .gallery-image.gallery-image_primary").attr("style","display:none !important");
		
		secondary_image_hover();
	})

	function secondary_image_hover(left, top)
	{

		(function() {
          var $section = jQuery('#inverted-contain');
            $section.find('.panzoom').panzoom({
            startTransform: 'scale(2)',
            increment: 1.0,
            minScale: 1,
            $zoomIn: $section.find(".zoom-in"),
            $zoomOut: $section.find(".zoom-out"),
            $reset: $section.find(".reset"),
            contain: 'invert'
          }).panzoom('zoom');
        })();

	}

var $panzoom = jQuery('#inverted-contain .panzoom');

$panzoom.on('panzoomend', function(e, panzoom, matrix, changed) {
  if (changed) {
    // deal with drags or touch moves
  } else {
  	jQuery('#inverted-contain .panzoom').panzoom("destroy");
  	jQuery('#inverted-contain').hide();

    jQuery(".product_placeholder_main.active .gallery-image_primary").show();
    
    
    
  }
});


jQuery("body").on("click",".reset",function(e){
	(function() {
      var $section = jQuery('#inverted-contain');
        $section.find('.panzoom').panzoom('destroy');
    })();
    jQuery('#inverted-contain').hide();

    jQuery(".product_placeholder_main.active .gallery-image_primary").show();


});


if (document.addEventListener) {
            document.addEventListener('webkitfullscreenchange', exitHandler, false);
            document.addEventListener('mozfullscreenchange', exitHandler, false);
            document.addEventListener('fullscreenchange', exitHandler, false);
            document.addEventListener('MSFullscreenChange', exitHandler, false);
        }
function exitHandler() {
        if (!document.webkitIsFullScreen && !document.mozFullScreen && !document.msFullscreenElement) {
           jQuery("#inverted-contain").hide();
           more_view_class = jQuery(".more-views").attr("class");
			product_shop_class = jQuery(".product-shop").attr("class");

			/*
				var more_additional_class = more_view_class.substring(11,more_view_class.length);
				var shop_additional_class = product_shop_class.substring(12,product_shop_class.length);
			*/
			more_view_style = jQuery(".more-views").attr("style");
			product_shop_style = jQuery(".product-shop").attr("style");

			console.log(more_view_style);
			console.log(more_view_class);
        }
    }

var e = jQuery.Event("keydown", {
    keyCode: 27
});
jQuery('body').on("keydown",function(e) {

    if (e.keyCode == 27) {
        jQuery("#inverted-contain").hide();
    }
    
});
})