jQuery(document).ready(function() {

	var owl_carousel_desktop = jQuery(".owl_carousel_desktop").owlCarousel(
	{
		margin:10,
	    nav:true,
	    items:1,
	    loop:true,
	    responsive:
	    {

	    }
	});

	/*jQuery('body').on("click",".owl_carousel_desktop_nav .owl-prev",function() {
		
	    owl.trigger('prev.owl.carousel',[500]);
	})
	
	jQuery('body').on("click",".owl_carousel_desktop_nav .owl-next",function() {
	    
	    owl.trigger('next.owl.carousel', [500]);
	})

	*/

	jQuery(".owl_carousel_desktop .owl-nav .owl-next").addClass("fa fa-chevron-circle-right");
	jQuery(".owl_carousel_desktop .owl-nav .owl-prev").addClass("fa fa-chevron-circle-left");

	/*Desktop Functionality starts here*/
	var current_image = jQuery(".swiper-slide.swiper-slide-active a img").attr("src");




	function singleZoomHandler(){
	    jQuery('#imageFullScreen').smartZoom('zoom', 3);
	}

	function singleZoomHandlerClose(){
		jQuery('#imageFullScreen').smartZoom('zoom', -2);
	}

	var zoom_page_click = function(){
    	
    		jQuery('#imageFullScreen').smartZoom({'containerClass':'zoomableContainer'});
			jQuery('#topPositionMap,#leftPositionMap,#rightPositionMap,#bottomPositionMap').bind("click", moveButtonClickHandler);
		    jQuery('#zoomInButton,#zoomOutButton').bind("click", zoomButtonClickHandler);	
		    jQuery('#imageFullScreen').smartZoom('zoom', 3);
	}


	jQuery(".zoom_full_product img").attr("src",current_image);


	jQuery("body").on("click",".swiper-button-next , .swiper-button-prev , .swiper-pagination-clickable .swiper-pagination-bullet", function(e){

		var current_image = jQuery(".swiper-slide.swiper-slide-active a img").attr("src");
		jQuery(".zoom_full_product img").attr("src",current_image);
	});

	jQuery("body").on("click",".swiper-slide > a",function(event){
		
		event.stopPropogation();
		
	})



	/*Desktop Functionality Ends here*/




	jQuery(".bakeri_img_box .bakeri_images").owlCarousel(
		{
			"loop":true,
		    "margin":0,
		    "nav":true,
		    "dots":true,
		    "items":1,
		    "responsive":{
		        0:{
		            items:1
		        },
		        600:{
		            items:1
		        }
		    },
		    thumbs:false,
		    

		});

	var owl = jQuery('.bakeri_img_box .bakeri_images');
	owl.owlCarousel();
	// Go to the next item
	jQuery('body').on("click",".bakeri_prev",function() {
		
	    owl.trigger('prev.owl.carousel',[500]);
	})
	// Go to the previous item
	jQuery('body').on("click",".bakeri_next",function() {
	    // With optional speed parameter
	    // Parameters has to be in square bracket '[]'
	    owl.trigger('next.owl.carousel', [500]);
	})
	


    



	function zoomButtonClickHandler(e){
	    var scaleToAdd = 1;
	    if(e.target.id == 'zoomOutButton')
	    scaleToAdd = -scaleToAdd;
	    jQuery('#imageFullScreen').smartZoom('zoom', scaleToAdd);
	    }
	    
	function moveButtonClickHandler(e){
	    var pixelsToMoveOnX = 0;
	    var pixelsToMoveOnY = 0;

	    switch(e.target.id){
	    case "leftPositionMap":
	    pixelsToMoveOnX = 50;
	    break;
	    case "rightPositionMap":
	    pixelsToMoveOnX = -50;
	    break;
	    case "topPositionMap":
	    pixelsToMoveOnY = 50;
	    break;
	    case "bottomPositionMap":
	    pixelsToMoveOnY = -50;
	    break;
	    }
	    jQuery('#imageFullScreen').smartZoom('pan', pixelsToMoveOnX, pixelsToMoveOnY);
	}

	var count_div = parseInt(1) ;
	
	var first_click =0;

	

	//jQuery('body').on('click', '.bakeri_arrow_expand , .bakeri_img_box .full_view_popup', function(){	
	jQuery('body').on('click', '.bakeri_arrow_expand , .bakeri_img_box .full_view_popup .owl-item', function(){	


	    //jQuery(".include_fullscreen").removeClass("hide_me");

	    jQuery(".include_fullscreen").removeClass("hide_me");

	    jQuery(".single_image_full_view").show();


		jQuery(".single_image_full_view").addClass("zoom_view");




		jQuery(".bakeri_plus_sign").attr("style","");
		jQuery(".bakeri_minus_sign").attr("style","");


		count_div =1;

		

		var bakeri_html = jQuery(".bakeri_product_zoom").html();


		

		
		jQuery(".bakeri_fullscreen").addClass('bakeri_display_block');
		jQuery(".bakeri_image_gallery").addClass('bakeri_zoom_gallery');
		jQuery(".slide-main").addClass('bakeri_main_image');
		jQuery(".slide-main").addClass('slide_zoom_main');
		jQuery(".bakeri_img_box").addClass('bakeri_display_none');
		jQuery(".bakeri_arrow_expand").addClass('bakeri_display_none');
		
        

        jQuery(".bakeri_image_gallery").addClass("bakery_cont_outer");
        


        //var owl_full_screen = jQuery('.bakeri_fullscreen .owl-carousel.bakeri_images');
		//owl.owlCarousel();
		var html_current_item = jQuery(".bakeri_img_box .full_view_popup .owl-item.active").html();

	    	jQuery(".single_image_full_view").prepend(html_current_item);
	    	jQuery(".single_image_full_view .bakeri_main_image").addClass("item_zoom");

	    	jQuery(".single_image_full_view").attr("id","imgContainer");

	    	jQuery(".item_zoom").attr("id","imageFullScreen");

	    	
	    	

	    	zoom_page_click();
		
        
        

        zoom_page_click();
        jQuery('.bakeri_message').show();
        setTimeout(function() {
            jQuery('.bakeri_message').hide();
         }, 2000);
    });

    

    jQuery('body').on('click','.bakeri_arrow_shrink', function(){	

    	jQuery(".include_fullscreen").addClass("hide_me");

    	setTimeout(function(){jQuery('#imageFullScreen').smartZoom('destroy');},300);

    	


    	
    	jQuery(".single_image_full_view").removeClass("zoom_view");
    	jQuery(".single_image_full_view .item").remove("");
    	jQuery(".single_image_full_view").attr("id","");
    	jQuery(".single_image_full_view").hide();

    	jQuery(".owl-thumbs.bakeri_images").hide();
    	jQuery(".bakery_slider_navs_thumb").hide();
    	

    	setTimeout(function(){
    		
    
    	jQuery(".bakeri_img_box .bakeri_images div").each(function(){
    		if(jQuery(this).hasClass("owl-thumbs"))
    		{
    			jQuery(this).remove();	
    		}

    	})
    	var main_slider_again = jQuery(".bakeri_img_box .bakeri_images").owlCarousel(
			{
				"loop":true,
			    "margin":0,
			    "nav":true,
			    "dots":true,
			    items:1,
			    "responsive":{
			        0:{
			            items:1
			        },
			        600:{
			            items:1
			        }
			    },
			    thumbs: false,
        		thumbsPrerendered: false,
			    

			});
    	main_slider_again.trigger("initialize.owl.carousel");


    	},100);
    	



    	jQuery('body').css('position','static');
		jQuery('body').css('overflow-y','visible');
        jQuery(".bakeri_fullscreen").removeClass('bakeri_display_block');
		jQuery(".bakeri_image_gallery").removeClass('bakeri_zoom_gallery');
		jQuery(".slide-main").removeClass('bakeri_main_image');
		jQuery(".slide-main").removeClass('slide_zoom_main');
		jQuery(".bakeri_img_box").removeClass('bakeri_display_none');
		jQuery(".bakeri_arrow_expand").removeClass('bakeri_display_none');
		jQuery(".bakeri_zoom_sign").removeClass('bakeri_display_block');
	    jQuery(".bakeri_zoom_sign").addClass('bakeri_display_none');
	    jQuery('.slide-main').removeClass('bakeri_first_zoom');
	    jQuery(".bakeri_more_views").removeClass('bakeri_display_block');
	    jQuery(".bakeri_more_views").addClass('bakeri_display_none');
	    jQuery('.bakeri_more_views').removeClass('bakeri_more_views_thumb');
	    jQuery(".bakeri_main_image").css({"left": "50%", "top": "50%"});
        jQuery(".bakeri_fullscreen").html('');

    	
    });
	//APPEND END

   
    
    //ZOOM START

    
    var count = 0;

    



    //THUMB START
	jQuery('body').on('click', '.bakeri_thumbs_link img', function(){
	    jQuery('.slide-main').attr('src',jQuery(this).attr('src').replace('thumb','1'));
	    jQuery(".bakeri_image_thumbs li").removeClass("thumb_active");
	    jQuery(this).parent().parent().addClass("thumb_active");
    });
    //THUMB END
      
   


   


    
    
    
	
});


  


