(function($){


	
	$(document).ready(function(){


		$("body").on("click","#ajaxlogin-mask",function(){
			//alert(0);
			//$(".ajaxlogin-window").hide();
			$('.ajaxlogin-window').hide();
			$('#ajaxlogin-mask').remove(); 
		})

		var ua = navigator.userAgent,
		event = (ua.match(/iPad/i) || ua.match(/iPhone/)) ? "touchend" : "click";


		$(window).load(function(){
			if($(window).width()<768)
			{
				$(".mobile_accordian").first().children().children(".footer_accordian_data").addClass("active");
				$(".mobile_accordian").first().children().children(".footer_title").addClass("active");
			}
		})


		$(".mobile_accordian .footer_menu ").on(event , ".footer_title" , function(e){
			
			
			if($(this).hasClass("active"))
			{
				$(this).removeClass("active");
				$(this).siblings(".footer_accordian_data").removeClass("active");
				return;
			}
			
			$(".mobile_accordian .footer_menu .footer_title").removeClass("active");
			$(".mobile_accordian .footer_menu .footer_accordian_data").removeClass("active");

			$(this).addClass("active");
			$(this).siblings(".footer_accordian_data").addClass("active");	

			/*$("html,body").animate({
				scrollTop:$(this).offset().top-50
			},500);
			*/

			
		})


		/*$("#header-search").modal({
		  closeClass: 'icon-remove',
		  closeText: '!'
		});*/

		$(window).load(function(){
			$("body").addClass("sticky_body");
			$(".page-header").addClass("sticky_header");
		})



		$(window).scroll(function(){

			if($(window).scrollTop()>200)
			{
				$(".up_btn").addClass("active");
			}
			else
			{
				$(".up_btn").removeClass("active");	
			}
		})		

		$(".up_btn").click(function(){
			$("html,body").animate({
				scrollTop:0
			})
		})


		$(".suppliers_carousel").owlCarousel({
			items:5,
			loop:true,
			nav:true,
			dots:false,
			autoplay:true,
			autoplayTimeout:3000,
			autoplayHoverPause:true,
			center:true,
			stagePadding:30,
			margin:30,
			navText:["<i class='fa fa-chevron-circle-left'></i>","<i class='fa fa-chevron-circle-right'></i>"],
			responsive:{
				420:{
					items:3
				},
				767:{
					items:4
				},
				991:{
					items:5	
				}
			}
		});

		$('body').on("click", ".search_icon",function(event) {
		  $("#search_modal").modal({
			  escapeClose: true,
			  clickClose: false,
			  showClose: false
			});
		});
		
		$("body").on("click", ".jquery-modal #search_modal .custom_close" , function(){
			$(".jquery-modal #search_modal input").val("");
			$(this).attr("rel","modal:close");
			$(".aa-dropdown-menu").hide();
		})

		/*if($(window).width()<780)
		{
			$('.search_icon').attr("rel","modal:open");
		}*/


	})
	
})(jQuery);
