(function($){
	$(document).ready(function(e){
		if($(".block.block-layered-nav .currently").html() != undefined)
		{
			//console.log(0);
			var filter_data = $(".block.block-layered-nav .currently").html();
			var clear_data = $(".block.block-layered-nav .currently+.actions").html();
			$(".filter_container").append(filter_data+clear_data);
		}
		else
		{
			//console.log(1);
		}


		var offset_filter = $(".block.block-layered-nav").offset().top;
		var width_filter = $(".block.block-layered-nav").width();
		var left_filter = $(".block.block-layered-nav").offset().left;

		var header_height = $("#header").height(); 
		

		$(window).scroll(function(){
			if($(window).scrollTop() >= (offset_filter-header_height))
			{
				$(".block.block-layered-nav").addClass("sticky_filter");
				$(".block.block-layered-nav").attr("style","left:"+left_filter+"px;width:"+width_filter+"px;right:auto;top:"+header_height+"px");
				//$(".block.block-layered-nav").attr("style","left:0px;width:100%;top:"+header_height+"px");

			}
			else
			{
				$(".block.block-layered-nav").removeClass("sticky_filter");	
				$(".block.block-layered-nav").attr("style","");
			}
		})
	})
})(jQuery)