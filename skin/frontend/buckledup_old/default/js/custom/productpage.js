(function($){
	$(document).ready(function(){
		/*var offset_filter = $(".block.block-layered-nav").offset().top;
		var width_filter = $(".block.block-layered-nav").width();
		var left_filter = $(".block.block-layered-nav").offset().left;
		console.log(offset_filter);

		$(window).scroll(function(){
			if($(window).scrollTop() >= offset_filter)
			{
				$(".block.block-layered-nav").addClass("sticky_filter");
				$(".block.block-layered-nav").attr("style","left:"+left_filter+"px;width:"+width_filter+"px;right:auto;");

			}
			else
			{
				$(".block.block-layered-nav").removeClass("sticky_filter");	
				$(".block.block-layered-nav").attr("style","");
			}
		})
*/


		$(window).load(function() {
            setTimeout(function(e) {
                var cart_location_top = $(".add-to-cart-wrapper .add-to-box").offset().top;
                console.log(cart_location_top);
                var cart_location_height = $(".add-to-cart-wrapper .add-to-box").height();
                var total_height_inner = parseInt(cart_location_top) + parseInt(cart_location_height);
                var total_height = parseInt(total_height_inner) - ($(window).height());

                if ($(window).height() > total_height_inner) {
                    $(".add-to-cart-wrapper .add-to-box").removeClass("fixed_cart");
                } else {
                    $(".add-to-cart-wrapper .add-to-box").addClass("fixed_cart");
                }
            }, 300)


            setTimeout(function(e) {
                var cart_location_top = $(".product-options-bottom .add-to-cart").offset().top;
                console.log(cart_location_top);
                var cart_location_height = $(".product-options-bottom .add-to-cart").height();
                var total_height_inner = parseInt(cart_location_top) + parseInt(cart_location_height);
                var total_height = parseInt(total_height_inner) - ($(window).height());

                if ($(window).height() > total_height_inner) {
                    $(".product-options-bottom .add-to-cart").removeClass("fixed_cart");
                } else {
                    $(".product-options-bottom .add-to-cart").addClass("fixed_cart");
                }
            }, 300)

        });
        setTimeout(function(e) {
            if($(".add-to-cart-wrapper ").children(".add-to-cart"))
            {
                var cart_location_top = $(".add-to-cart-wrapper .add-to-box").offset().top;
                var cart_location_height = $(".add-to-cart-wrapper .add-to-box").height();
                var total_height_inner = parseInt(cart_location_top) + parseInt(cart_location_height);
                var total_height = parseInt(total_height_inner) - ($(window).height());
                $(window).scroll(function(e) {
                    if ($(window).scrollTop() > total_height) {
                        $(".add-to-cart-wrapper .add-to-box").removeClass("fixed_cart");
                    } else {
                        $(".add-to-cart-wrapper .add-to-box").addClass("fixed_cart");
                    }
                })
            }
        }, 300);


        setTimeout(function(e) {
            if($(".product-options-bottom ").children(".add-to-cart"))
            {
                var cart_location_top = $(".product-options-bottom .add-to-cart").offset().top;
                var cart_location_height = $(".product-options-bottom .add-to-cart").height();
                var total_height_inner = parseInt(cart_location_top) + parseInt(cart_location_height);
                var total_height = parseInt(total_height_inner) - ($(window).height());
                $(window).scroll(function(e) {
                    if ($(window).scrollTop() > total_height) {
                        $(".product-options-bottom .add-to-cart").removeClass("fixed_cart");
                    } else {
                        $(".product-options-bottom .add-to-cart").addClass("fixed_cart");
                    }
                })
            }
        }, 300);


        $(".reduce_quantity").click(function(e){
            var value = $("#qty").val();
            if(value > 0)
            {
                $("#qty").attr("value",(parseInt(value)-1));    
            }
            else
            {
                $(".reduce_quantity").addClass("disabled");
            }
            
        })
        $(".increase_quantity").click(function(e){
            var value = $("#qty").val();
            $("#qty").attr("value",(parseInt(value)+1));
        })




	})
})(jQuery)
