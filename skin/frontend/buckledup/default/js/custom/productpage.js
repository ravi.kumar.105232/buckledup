(function($){
	$(document).ready(function(){
		$(".mini-products-list").owlCarousel({
			items:4,
			autoplay:true,
			autoplayTimeout:3500,
			autoplayHoverPause:true,
			margin:15,
			loop:true,
			navText:['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
			responsive : {
				0 : {
			        items:2,
			    	autoplayTimeout:2500
			    },
			    // breakpoint from 480 up
			    560 : {
			        items:3,
			    	autoplayTimeout:2500
			    },
			    // breakpoint from 768 up
			    768 : {
			           items:4,
			    		autoplayTimeout:2500
			    
			    }
			}
		})	
	})
	
})(jQuery)