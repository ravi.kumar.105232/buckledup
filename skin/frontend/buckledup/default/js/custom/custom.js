(function($) {
    $(document).ready(function() {

        $(window).load(function() {

            $("body").addClass("sticky_body ");
            $("header").addClass("sticky_header animate_all");
            /*    
            if ($(window).width() > 768) {
                $("body").addClass("sticky_body ");
                $("header").addClass("sticky_header animate_all");
            } else {
                $("body").removeClass("sticky_body ");
                $("header").removeClass("sticky_header animate_all");
            }
            */
        });

/*
        $(window).scroll(function() {
            if ($(window).width() > 767) {
                if ($(window).scrollTop() > 1) {
                    $("body").addClass("sticky_body ");
                    $("header").addClass("sticky_header animate_all");
                } else {
                    $("body").removeClass("sticky_body ");
                    $("header").removeClass("sticky_header animate_all");
                }
            }
        });*/
        $('.featured_product_mob_car').owlCarousel({
            margin: 10,
            items: 4,
            autoWidth: false,
            navText: ["<", ">"],
            responsive: {
                0: {
                    items: 1,
                    nav: true,
                    margin: 5,
                    loop: true,
                    autowith: true
                },
                600: {
                    items: 2,
                    nav: false,
                    autowith: true
                },
                991: {
                    items: 3,
                    nav: false
                },
                1024: {
                    items: 4,
                    nav: false
                }

            }

        })
        $("body").on("click", ".accordian_container .accordian_inner .accordain-head", function(e) {
            if ($(this).siblings(".accordain-content").hasClass("active_accordian_content")) {
                $(this).siblings(".accordain-content").toggleClass("active_accordian_content");
                $(this).parent(".accordian_inner").toggleClass("active_accordian");
            } else {
                $(".accordian_container .accordian_inner .accordain-content").removeClass("active_accordian_content");
                $(".accordian_container .accordian_inner").removeClass("active_accordian");
                $(this).siblings(".accordain-content").addClass("active_accordian_content");
                $(this).parent(".accordian_inner").addClass("active_accordian");
            }
        });
        var client_owl = $(".client_carousel_swiper");
        client_owl.owlCarousel({
            loop: true,
            items: 6,
            nav: true,
            dots: false,
            margin: 10,
            
            navText: ["", ""],
            responsive: {
                1366: {
                    items: 6,
                    slideBy:4,
                },
                767: {
                    items: 6,
                    slideBy:4,
                },
                540: {
                    items: 4,
                    slideBy:2,
                },
                320: {
                    items: 2,
                    slideBy:2,
                }

            },
            autoplay: false,
            autoplayTimeout: 1000,


        });


        $("#user_login").click(function(event) {
            $(".login_list").toggle();
        })
        $(".login_list").blur(function() {
            $(".login_list").close();
        })

        /*var slider = $('#slider');

        var thumbnailSlider = $('#thumbnailSlider');

        var duration = 500;


        slider.owlCarousel({
            loop: true,
            dots:false,
            nav: false,
            items: 1,
            rtl:false,
        }).on('changed.owl.carousel', function(e) {

            thumbnailSlider.trigger('to.owl.carousel', [e.item.index, duration, true]);
        });

        thumbnailSlider.owlCarousel({
 
            dots:false,
            loop: true,
            center: false, 
            nav: false,
            responsive: {
                0: {
                    items: 3
                },
                600: {
                    items: 4
                },
                1000: {
                    items: 6
                }
            }
        }).on('click', '.owl-item', function() {

            slider.trigger('to.owl.carousel', [$(this).index(), duration, true]);

        }).on('changed.owl.carousel', function(e) {
            slider.trigger('to.owl.carousel', [e.item.index, duration, true]);
        });
*/
    
            $('.slider-for').slick({
              slidesToShow: 1,
              slidesToScroll: 1,
              arrows: false,
              fade: true,
              asNavFor: '.slider-nav'
            });
            $('.slider-nav').slick({
              slidesToShow: 6,
              slidesToScroll: 1,
              asNavFor: '.slider-for',
              dots: true,
              centerMode: true,
              focusOnSelect: true,
              responsive:[
                {
                  breakpoint: 767,
                  settings: {
                    slidesToShow: 3,
                    centerMode:true

                  }
              },
                {
                      breakpoint: 480,
                      settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                      }
                                }
                ]
            });
          


        $('.slider-right').click(function() {
            slider.trigger('next.owl.carousel');
        });
        $('.slider-left').click(function() {
            slider.trigger('prev.owl.carousel');
        });


        var corporate_carousel = $('.coporate_carousel');
        corporate_carousel.owlCarousel({
            loop:true,
            dots:false,
            nav:true,
            items:6,
            margin:5,
            autoplay: false,
            autoplayTimeout: 3000,
            autoplayHoverPause:false,
            responsive:
            {
                0:{
                    items:2,
                    slideBy:2,
                },
                320:{
                    items:2,
                    slideBy:2,
                },
                540:{
                    items:4,
                    slideBy:2,
                },
                767:{
                    items:6,
                    slideBy:4,
                },
                1366:{
                    items:6,
                    slideBy:4,
                }

            }
        })


        

    })





})(jQuery);
