jQuery(document).ready(function() {

    /*Main images inner height Starts*/
    var prod_images_height = jQuery(".product-image").height();
    /*Main images inner height Ends*/


    jQuery(".image_thumb").click(function(e) {
        var id_get = jQuery(this).attr("data-ref");
        jQuery("html,body").animate({
            scrollTop: jQuery("#" + id_get).offset().top - offset_top,
        }, 800)
    });

    if (jQuery(window).width() < 767) 
    {

        var start_index;

        var bakery_image = jQuery(".bakeri_img_box .bakeri_images").owlCarousel({
            "loop": true,
            "margin": 0,
            "nav": true,
            "dots": true,
            "items": 1,
            "responsive": {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                }
            },
            thumbs: false,

        });
        bakery_image.on('changed.owl.carousel', function(event) {
            var currentItem = event.item.index;
            start_index = currentItem + 1;
        })
        

        /*Zoom Page Handler*/
        var zoom_page_click = function() {
            jQuery('#imageFullScreen').smartZoom({
                'containerClass': 'zoomableContainer'
            });
            jQuery('#zoomInButton,#zoomOutButton').bind("click", zoomButtonClickHandler);
            jQuery('#imageFullScreen').smartZoom('zoom', 0.9);
            var data_counter = jQuery(".bakeri_plus_sign").attr("data-counter");
            data_counter = parseInt(data_counter);
            jQuery(".bakeri_plus_sign").attr("data-counter", parseInt(data_counter + 1));
            var data_counter_lower = jQuery(".bakeri_minus_sign").attr("data-counter");
            jQuery(".bakeri_minus_sign").attr("data-counter", parseInt(data_counter_lower + 1));
            jQuery(".bakeri_minus_sign").removeClass("disable_it");
        }

        function zoomButtonClickHandler(e) {
            var scaleToAdd = 1;
            if (e.target.id == 'zoomOutButton')
                scaleToAdd = -scaleToAdd;
            jQuery('#imageFullScreen').smartZoom('zoom', scaleToAdd);
        }

        /*On Full screen Popup open for slider*/
        jQuery('body').on('click', '.bakeri_arrow_expand , .bakeri_img_box .full_view_popup .owl-item', function() {

            jQuery(".bakeri_fullscreen, .bakery_slider_navs_thumb").show();

            jQuery(".full_popup").owlCarousel({
                "loop": true,
                "margin": 0,
                "nav": true,
                "dots": false,
                "startPosition": start_index,
                "items": 1,
                "responsive": {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 1
                    }
                },
                "thumbs": true,
                "thumbsPrerendered": true,

            });

        });
        
        /*On Full screen Popup open for slider*/

        /*On Full screen Popup Closure for slider*/
        jQuery('body').on('click', '.bakeri_arrow_shrink', function() {
            jQuery(".bakeri_fullscreen, .bakery_slider_navs_thumb").hide();
            var full_popup = jQuery(".full_popup").owlCarousel();
            full_popup.trigger("destroy.owl.carousel");

        });
        
        /*On Full screen Popup Closure for slider*/

        jQuery("body").on("click", ".bakeri_fullscreen .full_popup .owl-item .gallery-image", function(e) {

            var html_current_item = jQuery(".bakeri_fullscreen .owl-item.active .item").html();
            jQuery(".zoomableContainer").remove();
            jQuery(".single_image_full_view").prepend(html_current_item);
            jQuery(".single_image_full_view, .single_image_popup").show();
            jQuery(".single_image_full_view").attr("id", "imgContainer");
            jQuery(".single_image_full_view .gallery-image").attr("id", "imageFullScreen");
            zoom_page_click();

        })

        jQuery("body").on("click", ".single_image_popup .bakeri_arrow_shrink", function(e) {

            var html_current_item = jQuery(".bakeri_fullscreen .owl-item.active .item").html();
            jQuery(".single_image_full_view").prepend(html_current_item);
            jQuery(".single_image_full_view, .single_image_popup").hide();
            jQuery(".single_image_full_view").attr("id", "");
            jQuery(".single_image_full_view .gallery-image").attr("id", "");
            zoom_page_click();

        })

        var check_count_counter = parseInt(0);
        jQuery(".bakeri_plus_sign").on("click", function(e) {

            var data_counter_lower = jQuery(".bakeri_minus_sign").attr("data-counter");
            jQuery(".bakeri_minus_sign").attr("data-counter", (parseInt(data_counter_lower) + 1));
            jQuery(".bakeri_minus_sign").removeClass("disable_it");

            var data_counter = jQuery(this).attr("data-counter");
            data_counter = parseInt(data_counter) + 1;
            jQuery(this).attr("data-counter", parseInt(data_counter));
            if (data_counter >= 2) {
                jQuery(this).addClass("disable_it");
                
                jQuery(this).attr("data-counter", parseInt(2));
            } else {
                jQuery(this).removeClass("disable_it");
                jQuery(".bakeri_minus_sign").removeClass("disable_it");
            }
        })

        jQuery(".bakeri_minus_sign").on("click", function(e) {
            var data_counter = jQuery(".bakeri_plus_sign").attr("data-counter");
            jQuery(".bakeri_plus_sign").attr("data-counter", parseInt(data_counter - 1));
            jQuery(".bakeri_plus_sign").removeClass("disable_it");

            var data_counter_low = jQuery(this).attr("data-counter");
            data_counter = parseInt(data_counter) - 1;
            jQuery(this).attr("data-counter", parseInt(data_counter));
            if (data_counter <= 0) {
                jQuery(this).addClass("disable_it");
                
                jQuery(this).attr("data-counter", 2);
            } else {
                jQuery(this).removeClass("disable_it");
                jQuery(".bakeri_minus_sign").removeClass("disable_it");
            }

        })


        jQuery(".bakery_slider_navs_thumb .bakeri_thumb_prev").on("click", function(e) {

            var width_scroll = jQuery(".owl-thumbs.bakeri_images .owl-thumb-item").width();

            jQuery('.owl-thumbs.bakeri_images').animate({
                scrollLeft: jQuery('.owl-thumbs.bakeri_images').scrollLeft() - parseInt(width_scroll),
                easing: 'easeInOutQuart'
            }, 300);
        })

        jQuery(".bakery_slider_navs_thumb .bakeri_thumb_next").on("click", function(e) {

            var width_scroll = jQuery(".owl-thumbs.bakeri_images .owl-thumb-item").width();
            jQuery('.owl-thumbs.bakeri_images').animate({
                scrollLeft: jQuery('.owl-thumbs.bakeri_images').scrollLeft() + parseInt(width_scroll),
                easing: 'easeInOutQuart'
            }, 300);
        })

	}
    

    var offset_top;

    if (jQuery(".page-header").hasClass(".sticky_header")) {
        offset_top = jQuery(".page-header.sticky_header").height();
        
    } else {
        offset_top = jQuery(".page-header").height();
        
    }

    jQuery(window).scroll(function(event) {
        /*if (jQuery(window).width() > 767) {

            var sticky_header = jQuery(".page-header").height();
            var thumbs_height = jQuery(".product-image-thumbs").height();
            var total_thumb_height = thumbs_height + sticky_header;
            var header_height = jQuery(window).height();
            if (total_thumb_height > header_height) {
                
                jQuery(".product-image-thumbs").attr("style", "bottom:" + total_thumb_height + "px");
            }
        }*/

        
    })

    /*Bakery slider code for thumbnail scrool and description scroll*/
    /*Product page Descriptions and Thumbnail Positioning Starts Here */

    //Product right Description Starts
    var prod_desc_height = jQuery(".product-shop").height();
    var prod_desc_width = jQuery(".product-shop").width();
    prod_desc_width = prod_desc_width + 15;
    var prod_thumb_height = jQuery(".more-views").height();
    var prod_thumb_width = jQuery(".more-views").width();
    //Product right Description Ends

    //Product right Description Left Starts
    var prod_desc_left = jQuery(".product-shop").offset().left;
    var prod_thumb_left = jQuery(".more-views").offset().left;
    //Product right Description Left Ends

    //Product image Main Left Starts
    var prod_main_image_left = jQuery(".product-image.product-image-zoom.for_desktop").offset().left;
    //Product Product image Main Left Ends

    var prod_main_image_width = jQuery(".product-image-gallery").width();

    //Header Description Starts
    var header_height = jQuery("header").height();
    //Product right Description Starts

    //height of Total Height Starts
    var total_prod_desc_height = parseInt(header_height) + parseInt(prod_desc_height);
    var total_prod_thumb_height = parseInt(header_height) + parseInt(prod_thumb_height);
    //height of Total Height Ends

    //height of Window Starts
    var window_height = jQuery(window).height();
    //height of Window Ends

    //Main images inner height Starts
    var prod_images_height = jQuery(".desktop_hide .product-image").height();
    //Main images inner height Ends

    //Main images inner height Starts
    var total_prod_images_height = prod_images_height + header_height;
    //Main images inner height Ends



    var counter = 0;
    var previousScroll = 0;

    var previous_Scroll = 0;

    jQuery(window).scroll(function() {

        if (jQuery(window).width() > 767) {
            var window_Scroll = jQuery(window).scrollTop();
            var window_height = jQuery(window).height();
            var total_scroll_height = window_Scroll + window_height;

            var top_adjust_desc = total_prod_images_height - total_prod_desc_height;
            var top_adjust_thumb = total_prod_images_height - total_prod_thumb_height;

            var full_height_desc_main;
            var full_height_thumb_main;

            var currentScroll = jQuery(this).scrollTop();
            if (currentScroll > previousScroll) {
                var offset_desc_top = jQuery(".product-shop").offset().top;
                var offset_thumb_top = jQuery(".more-views").offset().top;

                full_height_desc_main = offset_desc_top + prod_desc_height;
                full_height_thumb_main = offset_thumb_top + prod_thumb_height;


                if(prod_desc_height > total_prod_images_height)
                {
                    var top_adjust_small =  header_height+ 50;
                    //var height_diff_prod_main = prod_desc_height - total_prod_images_height;
                    var height_diff_prod_main = header_height;
                    if (full_height_desc_main <= total_scroll_height) {

                        if(full_height_desc_main > total_prod_images_height)
                        {
                            jQuery(".product-image.product-image-zoom.for_desktop").removeClass("fixed_top_imp");
                            jQuery(".product-image.product-image-zoom.for_desktop").addClass("relative_desc");
                            jQuery(".product-image.product-image-zoom.for_desktop").attr("style", "left:" + 0 + "px;width:" + prod_main_image_width + "px !important;top:" + height_diff_prod_main + "px;");

                            jQuery(".more-views").removeClass("fixed_top_imp");
                            jQuery(".more-views").addClass("relative_desc");
                            jQuery(".more-views").attr("style", "left:" + 0 + "px;width:" + prod_thumb_width + "px !important;top:" + height_diff_prod_main + "px;");
                        }

                    }
                    else
                    {
                        jQuery(".product-image.product-image-zoom.for_desktop").addClass("fixed_top_imp");
                        jQuery(".product-image.product-image-zoom.for_desktop").removeClass("relative_desc");
                        jQuery(".product-image.product-image-zoom.for_desktop").attr("style", "left:" + prod_main_image_left + "px;top:" + top_adjust_small + "px;width:" + prod_main_image_width + "px !important");

                        jQuery(".more-views").addClass("fixed_top_imp");
                        jQuery(".more-views").removeClass("relative_desc");
                        jQuery(".more-views").attr("style", "left:" + prod_thumb_left + "px;top:" + top_adjust_small + "px;width:" + prod_thumb_width + "px !important");
                    }
                    

                }
                else
                {

                    if (total_scroll_height >= full_height_desc_main) {

                        if (prod_desc_height < window_height) {
                            jQuery(".product-shop").addClass("fixed_top_zero");
                            jQuery(".product-shop").removeClass("relative_desc");

                            jQuery(".product-shop").attr("style", "left:" + prod_desc_left + "px;width:" + prod_desc_width + "px !important");
                        } else 
                        {
                            

                            jQuery(".product-shop").addClass("fixed_desc");
                            jQuery(".product-shop").removeClass("relative_desc");
                            jQuery(".product-shop").attr("style", "left:" + prod_desc_left + "px;width:"+prod_desc_width+"px !important");
                        }

                    }

                    if ( total_scroll_height >= total_prod_images_height) {

                       
                        if (prod_desc_height < window_height) {
                            var top_adjust_small = (prod_images_height) - (window_height - header_height);
                            jQuery(".product-shop").removeClass("fixed_top_zero");
                            jQuery(".product-shop").addClass("relative_desc");
                            jQuery(".product-shop").attr("style", "left:" + 0 + "px;top:" + top_adjust_small + "px");
                        } else {
                            jQuery(".product-shop").removeClass("fixed_desc");
                            jQuery(".product-shop").addClass("relative_desc");
                            jQuery(".product-shop").attr("style", "left:" + 0 + "px;top:" + top_adjust_desc + "px");
                        }

                    }

                    if (total_scroll_height >= full_height_thumb_main) {
                        if (prod_thumb_height < window_height) {
                            jQuery(".more-views").addClass("fixed_top_zero");
                            jQuery(".more-views").removeClass("relative_desc");
                            jQuery(".more-views").attr("style", "left:" + prod_thumb_left + "px");
                        } else {
                            jQuery(".more-views").addClass("fixed_desc");
                            jQuery(".more-views").removeClass("relative_desc");
                            jQuery(".more-views").attr("style", "left:" + prod_thumb_left + "px");
                        }

                    }

                    if (total_prod_images_height <= total_scroll_height) {

                        if (prod_thumb_height < window_height) {
                            var top_adjust_small = (prod_images_height) - (window_height - header_height);
                            jQuery(".more-views").removeClass("fixed_top_zero");
                            jQuery(".more-views").addClass("relative_desc");
                            jQuery(".more-views").attr("style", "left:" + 0 + "px;top:" + top_adjust_small + "px");
                        } else {
                            jQuery(".more-views").removeClass("fixed_desc");
                            jQuery(".more-views").addClass("relative_desc");
                            jQuery(".more-views").attr("style", "left:" + 0 + "px;top:" + top_adjust_thumb + "px");
                        }

                    }
                    

                }

                

            } else {
                var offset_desc_top = jQuery(".product-shop").offset().top;
                var offset_thumb_top = jQuery(".more-views").offset().top;

                var new_scroll_height = total_scroll_height - window_height;
                var new_offset_height_desc = offset_desc_top - header_height;
                var new_offset_height_thumb = offset_thumb_top - header_height;

                full_height_desc_main = offset_desc_top + prod_desc_height;


                if(prod_desc_height > total_prod_images_height)
                {
                    var top_adjust_small =  header_height+ 50;
                    var height_diff_prod_main = prod_desc_height - total_prod_images_height;
                    if (full_height_desc_main <= total_scroll_height) {

                        if(full_height_desc_main > total_prod_images_height)
                        {
                            jQuery(".product-image.product-image-zoom.for_desktop").removeClass("fixed_top_imp");
                            jQuery(".product-image.product-image-zoom.for_desktop").addClass("relative_desc");
                            jQuery(".product-image.product-image-zoom.for_desktop").attr("style", "left:" + 0 + "px;width:" + prod_main_image_width + "px !important;top:" + height_diff_prod_main + "px;");

                            jQuery(".more-views").removeClass("fixed_top_imp");
                            jQuery(".more-views").addClass("relative_desc");
                            jQuery(".more-views").attr("style", "left:" + 0 + "px;width:" + prod_thumb_width + "px !important;top:" + height_diff_prod_main + "px;");
                        }

                    }
                    else
                    {
                        jQuery(".product-image.product-image-zoom.for_desktop").addClass("fixed_top_imp");
                        jQuery(".product-image.product-image-zoom.for_desktop").removeClass("relative_desc");
                        jQuery(".product-image.product-image-zoom.for_desktop").attr("style", "left:" + prod_main_image_left + "px;top:" + top_adjust_small + "px;width:" + prod_main_image_width + "px !important");

                        jQuery(".more-views").addClass("fixed_top_imp");
                        jQuery(".more-views").removeClass("relative_desc");
                        jQuery(".more-views").attr("style", "left:" + prod_thumb_left + "px;top:" + top_adjust_small + "px;width:" + prod_thumb_width + "px !important");
                    }

                }
                else
                {

                    if (jQuery(".product-shop").hasClass("fixed_desc")) {
                        var new_offset_top = jQuery(".product-shop").offset().top;
                        jQuery(".product-shop").addClass("relative_desc");
                        jQuery(".product-shop").removeClass("fixed_desc");
                        jQuery(".product-shop").attr("style", "left:" + 0 + "px;top:" + (new_offset_top - header_height) + "px");
                    }
                    if (jQuery(".product-shop").hasClass("fixed_top_zero")) {
                        if (prod_desc_height < window_height) {
                            var top_adjust_small = (total_prod_images_height) - (window_height - header_height);
                            jQuery(".product-shop").removeClass("fixed_top_zero");
                            jQuery(".product-shop").addClass("relative_desc");

                            jQuery(".product-shop").attr("style", "left:" + 0 + "px;top:" + top_adjust_small + "px");
                        }
                    }

                    if (new_offset_height_desc >= new_scroll_height) {

                        jQuery(".product-shop").addClass("relative_desc");
                        jQuery(".product-shop").removeClass("fixed_desc");
                        jQuery(".product-shop").attr("style", "left:" + 0 + "px;top:" + new_scroll_height + "px");
                    }

                    if (jQuery(".more-views").hasClass("fixed_desc")) {
                        var new_offset_top = jQuery(".more-views").offset().top;
                        jQuery(".more-views").addClass("relative_desc");
                        jQuery(".more-views").removeClass("fixed_desc");
                        jQuery(".more-views").attr("style", "left:" + 0 + "px;top:" + (new_offset_top - header_height) + "px");

                    }
                    if (jQuery(".more-views").hasClass("fixed_top_zero")) {
                        if (prod_thumb_height < window_height) {
                            var top_adjust_small = (total_prod_images_height) - (window_height - header_height);
                            jQuery(".more-views").removeClass("fixed_top_zero");
                            jQuery(".more-views").addClass("relative_desc");
                            jQuery(".more-views").attr("style", "left:" + 0 + "px;top:" + top_adjust_small + "px");
                        }
                    }

                    if (new_offset_height_thumb >= new_scroll_height) {
                        jQuery(".more-views").addClass("relative_desc");
                        jQuery(".more-views").removeClass("fixed_desc");
                        jQuery(".more-views").attr("style", "left:" + 0 + "px;top:" + new_scroll_height + "px");

                    }
                }

            }
            previous_Scroll = window_Scroll;

            previousScroll = currentScroll;

        }


    });

    /*Product page Descriptions and Thumbnail Positioning Ends Here */
    /*Bakery slider code for thumbnail scrool and description scroll Ends here*/

});