/*Bakery slider code for thumbnail scrool and description scroll*/
/*Product page Descriptions and Thumbnail Positioning Starts Here */
		
	/*Product right Description Starts*/
	var prod_desc_height = jQuery(".product-shop").height();
	var prod_thumb_height = jQuery(".more-views").height();
	/*Product right Description Ends*/

	/*Product right Description Left Starts*/
	var prod_desc_left = jQuery(".product-shop").offset().left;
	var prod_thumb_left = jQuery(".more-views").offset().left;
	/*Product right Description Left Ends*/
	
	/*Header Description Starts*/
	var header_height = jQuery("header").height();
	/*Product right Description Starts*/
	
	/*height of Total Height Starts*/
	var total_prod_desc_height = parseInt(header_height)+parseInt(prod_desc_height);
	var total_prod_thumb_height = parseInt(header_height)+parseInt(prod_thumb_height);
	/*height of Total Height Ends*/

	/*height of Window Starts*/
	var window_height = jQuery(window).height();
	/*height of Window Ends*/

	/*Main images inner height Starts*/
	var prod_images_height = jQuery(".product-image").height();
	/*Main images inner height Ends*/

	/*Main images inner height Starts*/
	var total_prod_images_height = prod_images_height + header_height;
	/*Main images inner height Ends*/

	var counter = 0;
	var previousScroll = 0;

	var previous_Scroll = 0;
		    
	jQuery(window).scroll(function () 
	{
		var window_Scroll = jQuery(window).scrollTop();
		var window_height = jQuery(window).height();
		var total_scroll_height = window_Scroll+ window_height;

		var top_adjust_desc = total_prod_images_height - total_prod_desc_height;
		var top_adjust_thumb = total_prod_images_height - total_prod_thumb_height;

		var full_height_desc_main ; 
		var full_height_thumb_main ; 

	    var currentScroll = jQuery(this).scrollTop();
	    if (currentScroll > previousScroll)
	    {
	        var offset_desc_top = jQuery(".product-shop").offset().top;
		    var offset_thumb_top = jQuery(".more-views").offset().top;

		    full_height_desc_main = offset_desc_top + prod_desc_height;
		    full_height_thumb_main = offset_thumb_top + prod_thumb_height;


			if(total_scroll_height >= full_height_desc_main)
			{
				
				if(prod_desc_height < window_height)
				{
					jQuery(".product-shop").addClass("fixed_top_zero");
					jQuery(".product-shop").attr("style","left:"+prod_desc_left+"px");
				}
				else
				{
					jQuery(".product-shop").addClass("fixed_desc");
					    jQuery(".product-shop").attr("style","left:"+prod_desc_left+"px");
				}
				
			}
			
			if(total_prod_images_height<= total_scroll_height)
			{
				if(prod_desc_height < window_height)
				{
					var top_adjust_small = (prod_images_height) - (window_height - header_height);
					jQuery(".product-shop").removeClass("fixed_top_zero");
					jQuery(".product-shop").addClass("relative_desc");
					    jQuery(".product-shop").attr("style","left:"+0+"px;top:"+top_adjust_small+"px");
				}
				else
				{
					jQuery(".product-shop").removeClass("fixed_desc");
					jQuery(".product-shop").addClass("relative_desc");
					jQuery(".product-shop").attr("style","left:"+0+"px;top:"+top_adjust_desc+"px");
				}
				
			}

			if(total_scroll_height >= full_height_thumb_main)
			{
				if(prod_thumb_height < window_height)
				{
					jQuery(".more-views").addClass("fixed_top_zero");
					jQuery(".more-views").removeClass("relative_desc");
					jQuery(".more-views").attr("style","left:"+prod_thumb_left+"px");
				}
				else
				{
					jQuery(".more-views").addClass("fixed_desc");
					jQuery(".more-views").removeClass("relative_desc");
					jQuery(".more-views").attr("style","left:"+prod_thumb_left+"px");
				}
				
			}
			
			if(total_prod_images_height<= total_scroll_height)
			{
				if(prod_thumb_height < window_height)
				{
					var top_adjust_small = (prod_images_height) - (window_height - header_height);
					jQuery(".more-views").removeClass("fixed_top_zero");
					jQuery(".more-views").addClass("relative_desc");
					    jQuery(".more-views").attr("style","left:"+0+"px;top:"+top_adjust_small+"px");
				}
				else
				{
					jQuery(".more-views").removeClass("fixed_desc");
					jQuery(".more-views").addClass("relative_desc");
					jQuery(".more-views").attr("style","left:"+0+"px;top:"+top_adjust_thumb+"px");
				}
				
			}

	    }
	    else 
	    {
	    	var offset_desc_top = jQuery(".product-shop").offset().top;
			var offset_thumb_top = jQuery(".more-views").offset().top;

			var new_scroll_height = total_scroll_height - window_height;
			var new_offset_height_desc = offset_desc_top - header_height;
			var new_offset_height_thumb = offset_thumb_top - header_height;



			if(jQuery(".product-shop").hasClass("fixed_desc"))
			{
				var new_offset_top = jQuery(".product-shop").offset().top;
				jQuery(".product-shop").addClass("relative_desc");
				jQuery(".product-shop").removeClass("fixed_desc");
				jQuery(".product-shop").attr("style","left:"+0+"px;top:"+(new_offset_top - header_height)+"px");
			}
			if(jQuery(".product-shop").hasClass("fixed_top_zero"))
			{
				if(prod_desc_height < window_height)
				{
					var top_adjust_small = (total_prod_images_height) - (window_height - header_height);
					jQuery(".product-shop").removeClass("fixed_top_zero");
					jQuery(".product-shop").addClass("relative_desc");
					    jQuery(".product-shop").attr("style","left:"+0+"px;top:"+top_adjust_small+"px");
				}
			}

			if(new_offset_height_desc >= new_scroll_height)
			{
				jQuery(".product-shop").removeClass("relative_desc");
				jQuery(".product-shop").removeClass("fixed_desc");
				jQuery(".product-shop").attr("style","left:"+0+"px;top:"+new_scroll_height+"px");    
			}


			if(jQuery(".more-views").hasClass("fixed_desc"))
			{
				var new_offset_top = jQuery(".more-views").offset().top;
				jQuery(".more-views").addClass("relative_desc");
				jQuery(".more-views").removeClass("fixed_desc");
				jQuery(".more-views").attr("style","left:"+0+"px;top:"+(new_offset_top - header_height)+"px");
				
			}
			if(jQuery(".more-views").hasClass("fixed_top_zero"))
			{
				if(prod_thumb_height < window_height)
				{
					var top_adjust_small = (total_prod_images_height) - (window_height - header_height);
					jQuery(".more-views").removeClass("fixed_top_zero");
					jQuery(".more-views").addClass("relative_desc");
					jQuery(".more-views").attr("style","left:"+0+"px;top:"+top_adjust_small+"px");
				}
			}

			if(new_offset_height_thumb >= new_scroll_height)
			{
				jQuery(".more-views").addClass("relative_desc");
				jQuery(".more-views").removeClass("fixed_desc");
				jQuery(".more-views").attr("style","left:"+0+"px;top:"+new_scroll_height+"px");
				
				    
			}
	    	
	    }
	    previous_Scroll = window_Scroll;

	    previousScroll = currentScroll;
	});

	/*Product page Descriptions and Thumbnail Positioning Ends Here */
/*Bakery slider code for thumbnail scrool and description scroll Ends here*/
